# Infrastructure in `YC` which `k8s` cluster. 

## Description
Infrastructure in `YC` which `k8s` cluster. 

## Resources:
* `service accunt` - 1pcs (which the following roles:)
    * `compute.admin`
    * `vpc.admin`
    * `k8s.admin`
    * `kms.admin`
    * `iam.serviceAccounts.admin`
    * `resource-manager.admin`
    * `k8s.cluster-api.cluster-admin`
* `vpc` - 1 pcs
* `subnet` - 6 pcs (in the following zones:)
    * `ru-central1-a` 2 pcs (`pablic` and `private`)
    * `ru-central1-b` 2 pcs (`pablic` and `private`)
    * `ru-central1-c` 2 pcs (`pablic` and `private`)
* `gateway` - 3 pcs (in the following zones:)
    * `ru-central1-a` 1 pcs (`pablic`)
    * `ru-central1-b` 1 pcs (`pablic`)
    * `ru-central1-c` 1 pcs (`pablic`)
* `compute instance` - 4 pcs
    * `gitlab`
    * `gitlab-runner0`
    * `gitlab-runner1`
    * `gitlab-runner2`
* `k8s cluster` - 1 pcs (which 3 workplane nodes in the following zones:)
    * `ru-central1-a` 1 pcs (`private`)
    * `ru-central1-b` 1 pcs (`private`)
    * `ru-central1-c` 1 pcs (`private`)

## Quick start

### 1. Login into `Terraform cloud`
```shell
terraform login
```

### 2. Init
```shell
terraform init
```

### 3. Set environment
```shell
terraform workspace select <ENVIRONMENT>
```

### 4. Deploy
```shell
terraform apply --auto-approve
```

## Author
Aleksandr Khaikin <aleksandr.devops@gmail.com>
