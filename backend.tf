terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.80.0"
    }
  }
  cloud {
    organization = "netology-khaikin"

    workspaces {
      tags = ["webapp"]
    }
  }
  required_version = ">= 1.3.2"
}
