resource "yandex_kms_symmetric_key" "k8s" {
  name              = "symetric-key-${terraform.workspace}"
  description       = "Symetric key for k8s ${terraform.workspace} cluster"
  default_algorithm = "AES_256"
  rotation_period   = "8760h"
  folder_id = var.folder_id
}

resource "yandex_iam_service_account" "k8s" {
  name        = "k8s-${terraform.workspace}"
  description = "Service account for ${terraform.workspace} k8s cluster"
}

resource "yandex_resourcemanager_folder_iam_binding" "k8s" {
 folder_id = var.folder_id
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s.id}"
 ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
 folder_id = var.folder_id
 role      = "container-registry.images.puller"
 members   = [
   "serviceAccount:${yandex_iam_service_account.k8s.id}"
 ]
}

resource "yandex_kubernetes_cluster" "main" {
  name        = "main-${terraform.workspace}"
  description = "Main ${terraform.workspace} k8s cluster"
  network_id = yandex_vpc_network.main.id

  master {
    regional {
      region = "ru-central1"

      location {
        zone      = var.zones[0]
        subnet_id = yandex_vpc_subnet.private[var.zones[0]].id
      }

      location {
        zone      = var.zones[1]
        subnet_id = yandex_vpc_subnet.private[var.zones[1]].id
      }

      location {
        zone      = var.zones[2]
        subnet_id = yandex_vpc_subnet.private[var.zones[2]].id
      }
    }

    version   = var.k8s_version
    public_ip = true
  }

  service_account_id      = yandex_iam_service_account.k8s.id
  node_service_account_id = yandex_iam_service_account.k8s.id

  labels = {
    name       = "main-${terraform.workspace}"
  }

  lifecycle {
    prevent_destroy = true
  }

  release_channel = "STABLE"
  network_policy_provider = "CALICO"

  kms_provider {
    key_id = yandex_kms_symmetric_key.k8s.id
  }

  depends_on = [yandex_kms_symmetric_key.k8s]
}

resource "yandex_kubernetes_node_group" "k8s" {
  cluster_id  = yandex_kubernetes_cluster.main.id
  name        = "k8s-${terraform.workspace}-node-group"
  description = "Node group for k8s ${terraform.workspace} cluster"
  version     = var.k8s_version

  labels = {
    "name" = "k8s-${terraform.workspace}-node-group"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = false
      subnet_ids         = [
        yandex_vpc_subnet.private[var.zones[0]].id,
        yandex_vpc_subnet.private[var.zones[1]].id,
        yandex_vpc_subnet.private[var.zones[2]].id
      ]
    }

    resources {
      memory = var.k8s_configuration[terraform.workspace].memory
      cores  = var.k8s_configuration[terraform.workspace].cores
    }

    boot_disk {
      type = var.k8s_configuration[terraform.workspace].disk_type
      size = var.k8s_configuration[terraform.workspace].disk
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "docker"
    }
  }

  scale_policy {
    fixed_scale {
      size = 3
    }
  }

  lifecycle {
    prevent_destroy = true
  }

  allocation_policy {
    location {
      zone = var.zones[0]
    }
    location {
      zone = var.zones[1]
    }
    location {
      zone = var.zones[2]
    }
  }
}