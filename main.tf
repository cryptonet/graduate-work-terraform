module "gitlab" {
  source = "./modules/instance"
  subnet_id = yandex_vpc_subnet.public[var.zones[0]].id
  platform_id = var.platform_id
  zone = var.zones[0]
  image = var.debian-11-base
  cores = var.gitlab_configuration.server.cores
  memory = var.gitlab_configuration.server.memory
  name = "gitlab0"
  host_user = var.host_user
  ssh_public_key = var.ssh_public_key
  nat = true
  disk_size = var.gitlab_configuration.server.disk
  disk_type = var.gitlab_configuration.server.disk_type
  instance_count = terraform.workspace == "prod" ? 1 : 0
}

module "gitlab-runner" {
  source = "./modules/instance"
  subnet_id = yandex_vpc_subnet.public[each.key].id
  platform_id = var.platform_id
  zone = var.zones[index(var.zones, each.key)]
  image = var.debian-11-base
  cores = var.gitlab_configuration.runner.cores
  memory = var.gitlab_configuration.runner.memory
  name = "gitlab-runner${index(var.zones, each.key)}"
  host_user = var.host_user
  ssh_public_key = var.ssh_public_key
  nat = true
  disk_size = var.gitlab_configuration.runner.disk
  disk_type = var.gitlab_configuration.runner.disk_type
  instance_count = terraform.workspace == "prod" ? 1 : 0
  for_each = toset(var.zones)
}
