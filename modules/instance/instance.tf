resource "yandex_compute_instance" "instance" {
  name                      = "${var.name}-${terraform.workspace}${count.index}"
  zone                      = var.zone
  hostname                  = var.name
  allow_stopping_for_update = true
  platform_id               = var.platform_id
  count                     = var.instance_count

  resources {
    cores  = var.cores
    memory = var.memory
  }

  boot_disk {
    initialize_params {
      image_id    = var.image
      name        = "root-${var.name}-${terraform.workspace}"
      type        = var.disk_type
      size        = var.disk_size
    }
  }

  lifecycle {
    prevent_destroy = true
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = var.nat
  }

  metadata = {
    ssh-keys = "${var.host_user}:${var.ssh_public_key}"
  }
}
