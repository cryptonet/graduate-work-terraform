variable "subnet_id" {
  type        = string
  description = "Subnet id"
}

variable "platform_id" {
  type        = string
  description = "Platform id"
}

variable "image" {
  type        = string
  description = "Image id"
}

variable "cores" {
  type = number
  description = "Number of cores"
}

variable "memory" {
  type = number
  description = "Number of memory in GB"
}

variable "disk_type" {
  type = string
  description = "Type of disk"
}

variable "disk_size" {
  type = number
  description = "Size of disk in GB"
}

variable "instance_count" {
  type = number
  description = "Count of instance"
}

variable "zone" {
  type        = string
  description = "Availability zone"
}

variable "nat" {
  type        = bool
  description = "Enable or disable NAT"
}

variable "name" {
  type        = string
  description = "Name of host"
}

variable "host_user" {
  type        = string
  description = "Username of host"
}

variable "ssh_public_key" {
  type        = string
  description = "SSH public key"
}
