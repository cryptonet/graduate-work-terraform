resource "yandex_vpc_network" "main" {
  name = "main-${terraform.workspace}"
  description = "Main ${terraform.workspace} network"

  lifecycle {
    create_before_destroy = true
  }
}

resource "yandex_vpc_subnet" "public" {
  name = "public${index(var.zones, each.key)}-${terraform.workspace}"
  description = "VPC public${index(var.zones, each.key)}-${terraform.workspace}"
  v4_cidr_blocks = [var.cidr_public[terraform.workspace][index(var.zones, each.key)]]
  zone           = var.zones[index(var.zones, each.key)]
  network_id     = yandex_vpc_network.main.id

  lifecycle {
    create_before_destroy = true
  }
  for_each = toset(var.zones)
}

resource "yandex_vpc_gateway" "gw" {
  name = "gw${index(var.zones, each.key)}-${terraform.workspace}"
  shared_egress_gateway {}
  for_each = toset(var.zones)
}

resource "yandex_vpc_route_table" "gw" {
  name       = "gw${index(var.zones, each.key)}-${terraform.workspace}"
  network_id = yandex_vpc_network.main.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.gw[each.key].id
  }
  for_each = toset(var.zones)
}

resource "yandex_vpc_subnet" "private" {
  name = "private${index(var.zones, each.key)}-${terraform.workspace}"
  v4_cidr_blocks = [var.cidr_private[terraform.workspace][index(var.zones, each.key)]]
  zone           = var.zones[index(var.zones, each.key)]
  network_id     = yandex_vpc_network.main.id
  route_table_id = yandex_vpc_route_table.gw[each.key].id

  lifecycle {
    create_before_destroy = true
  }
  for_each = toset(var.zones)
}
