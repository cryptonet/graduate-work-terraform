output "gitlab_external_ip_address" {
  value = module.gitlab[*].external_ip_address
  depends_on = [module.gitlab]
}

output "gitlab_internal_ip_address" {
  value = module.gitlab[*].internal_ip_address
  depends_on = [module.gitlab]
}

output "gitlab-runner_ip_addresses" {
  value = {
    for k, v in module.gitlab-runner : k => v
  }
  depends_on = [module.gitlab-runner]
}
