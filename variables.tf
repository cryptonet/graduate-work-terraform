variable "debian-11-base" {
  description = "Debian-11 image id"
  type = string
  default = "fd8m8s42796gm6v7sf8e"
}

variable "host_user" {
  description = "Host user"
  type = string
}

variable "platform_id" {
  description = "Yandex platform id"
  type = string
  default = "standard-v1"
}

variable "cloud_id" {
  description = "Yandex cloud id"
  type = string
}

variable "folder_id" {
  description = "Yandex folder id"
  type = string
}

variable "ssh_public_key" {
  description = "SSH public key"
  type = string
}

variable "zones" {
  description = "Yandex cloud available zones"
  type = list(string)
  default = ["ru-central1-a", "ru-central1-b", "ru-central1-c"]
}

variable "cidr_public" {
  type = map(list(string))
  default = {
    stage    = ["192.168.10.0/24", "192.168.11.0/24", "192.168.12.0/24"]
    prod     = ["192.168.20.0/24", "192.168.21.0/24", "192.168.22.0/24"]
  }
}

variable "cidr_private" {
  type = map(list(string))
  default = {
    stage    = ["192.168.30.0/24", "192.168.31.0/24", "192.168.32.0/24"]
    prod     = ["192.168.40.0/24", "192.168.41.0/24", "192.168.42.0/24"]
  }
}

variable "k8s_version" {
  description = "K8S cluster version"
  type = string
  default = "1.22"
}

variable "k8s_configuration" {
    type = map(object({
    cores = number
    memory = number
    disk = number
    disk_type = string
  }))

  default = {
    stage  = {
      cores = 2
      memory = 4
      disk = 30
      disk_type = "network-hdd"
    }
    prod   = {
      cores = 4
      memory = 8
      disk = 50
      disk_type = "network-nvme"
    }
  }
}

variable "gitlab_configuration" {
  type = map(object({
    cores = number
    memory = number
    disk = number
    disk_type = string
  }))

  default = {
    server  = {
      cores = 4
      memory = 8
      disk = 50
      disk_type = "network-nvme"
  }
  runner   = {
    cores = 2
    memory = 4
    disk = 30
    disk_type = "network-nvme"
  }
  }
}
